#include<stdlib.h>
#include<stdio.h>

struct _list {
    int val;
    struct _list* next;
};

typedef struct _list List;

int cons(List** lst, int val) {
    List* new = malloc(sizeof(List));
    if ( !new ) return -1;
    new->val = val;
    new->next = *lst;
    *lst = new;
    return 0;
}

int _insert_ordered(List** dst, int val) {
    List* head = *dst;
    if ( !head || head->val > val )
        return cons(dst, val);
    else
        return _insert_ordered(&head->next, val);
}

int insert_sort(List** dst, List* src) {
    if ( ! src ) return 0;
    _insert_ordered(dst, src->val);
    return insert_sort(dst, src->next);
}


/* this function splits the list
 * by two parts
 * the first argument - src1 - is a current element to be split-off
 * the second argument - src2 - is end-finder element when it becomes
 *  NULL -- stop the splitting
 */
int _list_split(List* src1, List* src2, List** left, List** right) {
    if (!src2 || !src2->next) {
        *right = src1;
        return 0;
    } else {
        cons(left, src1->val);
        return _list_split(src1->next, src2->next->next, left, right);
    }
}

/* 'interface' function which starts
 * recursion with needed parameters:
 * two src lists (one for splitting and one for end-finding)
 *
 */
int list_split(List* src, List** left, List**right) {
    return _list_split(src, src, left, right);    
}

/* to make merge effective, it should be tail-recursive
 * but, in this case we lost functional purity
 */
int list_merge(List** dst, List* src1, List* src2 ) {
    if (!src1 && !src2) {
        /* both lists are empty,
         * so, do nothing
         */
        return 0;
    } else if ( !src1  ||  (src2 && src1->val >= src2->val) ) {
        /* the case where we shoud take the value
         * from src2 and iterate over it
         */
        *dst = src2;
        if ( src1 ) /* if src1 is not null, let's go through it */
            return list_merge(&(*dst)->next, src1, src2->next);
    } else if ( !src2 || (src1 && src1->val < src2->val) ) {
        /* the case where we should take the value
         * from src1,
         */
        *dst = src1;
        if ( src2 )
            return list_merge(&(*dst)->next, src1->next, src2);
    }
    return 0;
}

int quick_sort(List** dst, List* src) {
    List* left = NULL, *right = NULL;
    List* lsorted = NULL, *rsorted = NULL;
    if ( !src) return 0;
    if ( !src->next ) {
            return cons(dst, src->val);
    }
    list_split(src, &left, &right);
    quick_sort(&lsorted, left);
    quick_sort(&rsorted, right);
    return list_merge(dst, lsorted, rsorted);
}


int take(List** lst, int* val) {
    List* top  = *lst;
    if ( top ) {
        *val = top->val;
        *lst = top->next;
        free(top);
        return 0;
    }
    return -1;
}

void list_free(List* lst) {
    int val;
    while (!take(&lst, &val)) {}
}


int main() {
    List* a = NULL;
    List *x = NULL;
    List *y = NULL;
    List* sorted = NULL;
    int val;
    while ( scanf("%d", &val)) {
        cons(&a, val);
    }
    quick_sort(&sorted, a);
    printf("List x:\n");
    while ( !take(&sorted, &val) ) {
        printf("%d\n", val);
    }

    list_free(a);
    list_free(sorted);
}
