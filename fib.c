#include<stdio.h>
#include<string.h>

void mprint(int* m) {
	printf("%d %d\n", m[0], m[1]);
	printf("%d %d\n", m[2], m[3]);
}



void mmul(int *m, int *mx) {
	int res[4];
	res[0] = m[0]*mx[0] + m[1]*mx[2];
	res[1] = m[0]*mx[1] + m[1]*mx[3];
	res[2] = m[2]*mx[0] + m[3]*mx[2];
	res[3] = m[2]*mx[1] + m[3]*mx[3];
	memcpy(m, res, 4*sizeof(int));
}

void msquare(int* m) {
	mmul(m, m);
}

void fill_ident(int* m) {
	m[0] = 1;
	m[1] = 0;
	m[2] = 0;
	m[3] = 1;
}

void mvec(int* m, int* v) {
	int res[2];
	res[0] = m[0]*v[0] + m[1]*v[1];
	res[1] = m[2]*v[0] + m[3]*v[1];
	v[0] = res[0];
	v[1] = res[1];
}
	

void mpow2(int* m, int n) {
	printf("calculating for power %d\n",n);
	if ( n == 0 ) {
		fill_ident(m);
	} else if ( n == 1)  {
	} else if ( n % 2 == 0) {
		mpow2(m, n/2);
		msquare(m);
	} else {
		int mx[4];
		memcpy(mx, m, 4*sizeof(int));
		mpow2(m, (n-1)/2);
		msquare(m);
		mmul(m, mx);
	}
}


int pow2(int a, int n) {
	int x;
	if ( n == 0 ) return 1;
	if ( n == 1 ) return a;
	if ( n % 2 == 0) {
		x = pow2(a, n/2);
		return x*x;
	} else {
		x = pow2(a, (n-1)/2);
		return x*x*a;
	}
}



void _fibn(int n, int* p, int* pp) {
	if ( n <= 1) {
		*p = 1;
		*pp = 1;
	} else {
		int c;
		_fibn(n-1, p, pp);
		c = *pp;
		*pp = *p;
		*p = *p + c;
	}
}

int fibn(int n) {
	int p, pp;
	_fibn(n, &p, &pp);
	return p;	
}

int fib(int n) {
	if ( n <= 1) {
		return 1;
	} else {
		return fib(n-1)+fib(n-2);
	}
}

int main() {
	int d;
	while(1) {
		printf("no of fib num: ");
		scanf("%d", &d);
		if ( d > 0) {
			int v[2] = {1,0};
			int m[4] = {1,1,1,0};
			mpow2(m, d);
			mvec(m, v);
			printf("Fib(%d) = %d\n", d, v[0]);
		} else {
			break;
		}
	}
}
