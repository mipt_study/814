#include <stdio.h>

char A[10][10][10];

union VAR {
	double d;
	char z[8];
	int a;
};

struct Array1 {
	int a :1;
	int b :2;
	char z[10];
} __attribute__((packed));

struct Array2 {
	int a;
	int b;
	char z[];
};

int main() {
	union VAR x = { 0};
	int i;
	x.d = 0.0;
	//x.a = 0121;
	//x.z[6]=10;
	//for (i = 0; i < 8; i++ ) x.z[i] = 0x00;
	x.z[7] = 0x80;
	printf("Size Array1: %d\n", sizeof(struct Array1));
	printf("Size Array2: %d\n", sizeof(struct Array2));
	printf("Size VAR: %d\n", sizeof(union VAR));
	printf("VAR bytes: %x %x %x %x %x %x %x %x\n",
		x.z[0],
		x.z[1],
		x.z[2],
		x.z[3],
		x.z[4],
		x.z[5],
		x.z[6],
		x.z[7]);
	printf("VAR int feild: %d\n", x.a);
	printf("VAR double field: %.15lf\n", x.d);

	while(1) {
		int i,j,k;
		printf("enter i,j of array ");
		scanf("%d %d %d", &i, &j, &k);
		printf("Address of A[%d][%d][%d] = %p\n",
			i, j, k,  &A[i][j][k]);

	}

}
