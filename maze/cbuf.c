#include "cbuf.h"
#include <stdlib.h>

struct _node {
	Data data;
	struct _node* next;

};

int cbuf_insert(Node** buf, Data data) {
	Node* new = malloc(sizeof(Node));
	if ( !new ) return -1;
	new->data = data;
	if ( *buf ) {
		Node* next = (*buf)->next;
		Node* old = next->next;
		next->next = new;
		new->next = old;
		*buf = next;
	} else {
		new->next = new;
		*buf = new;
	}
	return 0;
}

int cbuf_take(Node** buf, Data* data) {
	if(*buf) {
		Node* last = (*buf)->next;
		Node* first = last->next;
		*data = first->data;
		last->next = first->next;
		if ( *buf == first ) {
			if ( last != first ) {
				*buf = last;
			} else {
				*buf = NULL;
			}
		}
		free(first);
		return 1;
	}
	return 0;
}

