#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"cbuf.h"

struct _Maze {
	int w, h;
	int exit_x, exit_y;
	int enter_x, enter_y;
	int cells[];
};

typedef struct _Maze Maze;


int estimate_size(FILE* maze_file, int* w, int* h) {
	char buf[1024];
	*h = 0;
	*w = 0;
	while ( !feof(maze_file)) {
		fgets(buf, 1024, maze_file);
		(*h)++;
		*w = strlen(buf)-1 > *w ? strlen(buf)-1 : *w;
	}
	printf("Maze read: %d x %d\n", *w, *h);
	return 0;
}

int read_maze(Maze* m, FILE* maze_file) {
	char buf[1024];
	int j = 0;
	while( !feof(maze_file)) {
		int i;
		fgets(buf, 1024, maze_file);
		for (i = 0; i < strlen(buf) && buf[i] != '\n'; i++ ) {
			switch (buf[i]) {
			case '#':
				*(m->cells + m->w * j + i) = -1;
				break;
			case 'I':
				m->enter_x = i;
				m->enter_y = j;
				*(m->cells + m->w * j + i)  = 0;
				break;
			case 'O':
				m->exit_x = i;
				m->exit_y = j;
				break;
			default:
				*(m->cells + m->w * j + i)  = 0;
			}	

		}
		j++;

	} 	
}

int maze_print(Maze* m) {
	int i, j;
	for ( j = 0; j < m->h; j++ ) {
		for ( i = 0; i < m->w; i++ ) {
			if ( i == m->enter_x && j == m->enter_y ) {
				printf(" I ");
			} else if ( i == m->exit_x && j == m->exit_y ) {
				printf(" O ");
			} else {
				switch(m->cells[m->w*j + i]) {
				case 0:
					printf("   ");
					break;
				case -1:
					printf("###");
					break;
				default:
					printf("%3d", m->cells[m->w*j + i]);

				}
			}
		}
		puts("");
	}
	puts("");
	return 0;
}

int maze_get(Maze* m, int x, int y) {
	if ( x >= 0 && x < m->w
		&& y >= 0 && y < m->h ) 
		return m->cells[m->w * y + x];
	return 0;
}

void maze_set(Maze* m, int x, int y, int pos) {
	if ( x >= 0 && x < m->w
		&& y >= 0 && y < m->h ) 
		m->cells[m->w * y + x] = pos;
}


int fill_maze(Maze** m, char* filename) {
	FILE* maze_file;
	int err = 0;
	int w = 0, h = 0, j;
	maze_file = fopen(filename, "r");
	*m = NULL;
	if ( !maze_file ) return -1;

	estimate_size(maze_file, &w, &h);

	rewind(maze_file);

	*m = malloc(sizeof(Maze) + w * h * sizeof(int));
	memset(*m, 0, sizeof(Maze) + w * h * sizeof(int));

	(*m)->w = w;
	(*m)->h = h;

	read_maze(*m, maze_file);

exit:
	fclose(maze_file);
	return err;

}

#define PACK_COORDS(x, y) ((x) << 16 | (y))
#define UNPACK_X(packed) ((packed) >> 16)
#define UNPACK_Y(packed) ((packed) & 0xFFFF)

void find_path_broad(Maze* m, int x, int y, int step, int* result) {
	Node* buf = NULL;
	int coords;
	cbuf_insert(&buf, PACK_COORDS(x, y));
	maze_set(m, x, y, step);
	while (cbuf_take(&buf, &coords)) {
		int cx = UNPACK_X(coords);
		int cy = UNPACK_Y(coords);
		step = maze_get(m, cx, cy) + 1;
		if ( cx == m->exit_x && cy == m->exit_y ) {
			*result = step;
			break;
		}	
		if ( check_move(m, cx+1, cy, step) ) 
			cbuf_insert(&buf, PACK_COORDS(cx+1, cy));
		if ( check_move(m, cx-1, cy, step) ) 
			cbuf_insert(&buf, PACK_COORDS(cx-1, cy));
		if ( check_move(m, cx, cy+1, step) )
			cbuf_insert(&buf, PACK_COORDS(cx, cy+1));
		if ( check_move(m, cx, cy-1, step) )
			cbuf_insert(&buf, PACK_COORDS(cx, cy-1));
	}
	while(cbuf_take(&buf, &coords)) {}
}


void find_path(Maze* m, int x, int y, int step, int* result) {
	if ( x == m->exit_x && y == m->exit_y )  {
		if ( *result == 0 || *result > step )
			*result = step;
		return;	
	}
		
	if( x >= 0 && y >= 0 
		&& x < m->w && y < m->h ) {
		int pos = maze_get(m, x, y);
		if ( pos == 0 || pos > step ) {
			maze_set(m, x, y, step);
			find_path(m, x+1, y, step+1, result);
			find_path(m, x-1, y, step+1, result);
			find_path(m, x, y+1, step+1, result);
			find_path(m, x, y-1, step+1, result);
		}
	}
}

int main() {
	Maze* m = NULL;
	int result;
	if ( !fill_maze(&m, "maze.txt") ) {
		maze_print(m);
		/*find_path(m, m->enter_x, m->enter_y, 1, &result); */
		find_path_broad(m, m->enter_x, m->enter_y, 1, &result);
		maze_print(m);
		printf("Shortest path: %d\n", result);
	} else {
		printf("error while reading maze\n");
	}
}

