#include "cbuf.h"
#include <stdio.h>

int main() {
	Node* buf = NULL;
	int i, result;
	for ( i = 1; i <= 10; i++ ) {
		if ( cbuf_insert(&buf, i) ) {
			printf("Error while inserting element %d\n", i);
			while(cbuf_take(&buf, &result)) {}
			return -1;
		}
		
	}

	for ( i = 0; i < 3; i++ ) {
		cbuf_take(&buf, &result);
		printf("Taken: %d\n", result);
	}
	while(cbuf_take(&buf, &result)) {}
	return 0;
}
