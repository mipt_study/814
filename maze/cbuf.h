#ifndef __CBUF_H__
#define __CBUF_H__

typedef int Data;

typedef struct _node Node;

int cbuf_insert(Node** buf, Data data);

int cbuf_take(Node** buf, Data* data);

#endif /* __CBUF_H__ */
