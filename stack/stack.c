typedef struct {
	size_t sz;
	size_t top;
	int* values;
} ArrayStack;

#define STACK_INIT_SZ 32


/*
 *
 *
 * ArrayStack s;
 *
 * ArrayStack* ps;
 * ps = malloc(sizeof(ArrayStack));
 *
 *
 * astack_init(ps);
 * for (i = 0; i < 100; i++ ) {
 *		astack_push(ps, i*i);
 * }
 * astack_pop(ps, &i);
 *
 * astack_free(ps);
 */

int astack_init(ArrayStack* stack) {
	stack->sz = STACK_INIT_SZ;
	stack->values = calloc(STACK_INIT_SZ, sizeof(int));
	stack->top = -1;
	return stack->values == NULL;
}

int astack_push(ArrayStack* stack, int val) {
	if (stack->sz <= stack->top)  {
		stack->sz *= 2;
		stack->values = realloc(stack->values, stack->sz * sizeof(int));
		if ( ! stack->values ) 
			return -1;
	}
	stack->values[++stack->top] = val;
	return 0;
}

int astack_peek(ArrayStack* stack, int elem, int* val) {
	if ( stack->top - elem >= 0 ) {
		*val = stack->values[stack->top - elem];
		return 0;
	}
	return -1;
}


int astack_pop(ArrayStack* stack, int* val) {
	if ( stack->top < 0 ) 
		return -1;
	*val = stack->values[stack->top--];
	return 0;
}

void astack_free(ArrayStack* stack) {
	free(stack->values);
}

/*
 *
 *	BlockStack* bs = NULL;
 *
 *	for ( i = 0; i < 100; i++ ){
 *		bstack_push(&bs, i*i);
 *	}
 *	bstack_pop(&bs, &i);
 *
 */


typedef struct _bstack BlockStack;
struct _bstack {
	int val;
	struct _bstack* next;	
};

int bstack_push(BlockStack** bstack, int val) {
	BlockStack* new = malloc(sizeof(BlockStack));
	if ( !new ) return -1;
	new->val = val;
	new->next =*bstack;
	*bstack = new;
	return 0;
}

int bstack_peek(BlockStack* bstack, int elem, int* val) {
	if ( !bstack )
		return -1;

	if ( elem == 0 )  {
		*val = bstack->val;
		return 0;
	}
	return bstack_peek(bstack->next, elem-1, val);
}

int bstack_pop(BlockStack** bstack, int* val) {
	BlockStack* top = *bstack;
	if ( top ) {
		*val = top->val;
		*bstack = top->next;
		free(top);
		return 0;
	}
	return -1;	
}

