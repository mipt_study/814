#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define TAPE_MAX 4096
#define STMTS_MAX 128

typedef char* TAPE;

struct _STATEMENT {
	char r;
	int q;
	char w;
	int qn;
	int dir;
};

typedef struct _STATEMENT STATEMENT;

typedef struct {
	int num;
	STATEMENT* statements;
} PROG;

struct _TURING {
	PROG* prog;
	TAPE tape;
	int q;
	int pos;
	int pos_min;
	int pos_max;
};

typedef struct _TURING TURING;


PROG* prog_alloc(int sz) {
	PROG* prog = malloc(sizeof(PROG));
	if (! prog ) return NULL;
	prog->statements = calloc(sizeof(STATEMENT), sz);
	if ( !prog->statements ) {
		free(prog);
		return NULL;
	}
	prog->num = 0;
	return prog;
}

void prog_free(PROG* prog) {
	free(prog->statements);
	free(prog);
}

int prog_read(TURING* machine, FILE* f) {
	int stmts = 0;
	while(!feof(f) && stmts < STMTS_MAX) {
		STATEMENT *s = &machine->prog->statements[stmts];
		char dir;
		if(fscanf(f, "%c %d %c %d %c ",
			&s->r, &s->q, &s->w, &s->qn, &dir) == 5) {
		
			printf("Read statement: '%c' %d -> '%c' %d %c\n",
				s->r, s->q, s->w, s->qn, dir);

			s->dir = dir=='<' ? -1 : 1;
			stmts++;
		}

	}
	printf("Statements read: %d\n", stmts);
	machine->prog->num = stmts;
	return 0;
}

int tape_set(TAPE tape, int pos, char val) {
	pos = pos < 0? -pos*2-1 : pos*2;
	if ( pos < TAPE_MAX ) {
		*(tape+pos) = val;
		return 0;
	}
	return -1;
}

int tape_get(TAPE tape, int pos, char* pval) {
	pos = pos < 0? -pos*2-1 : pos*2;
	if ( pos < TAPE_MAX ) {
		*pval = *(tape+pos);
		return 0;
	}
	return -1;
}		

int tape_read(TURING* machine, FILE* f)  {
	int offset;
	char val;
	if (fscanf(f, "%d ", &offset) != 1 ) {
		printf("Wrong file format! offset expected\n");
		return -11;
	}

	if ( offset < -TAPE_MAX/2  || offset > TAPE_MAX/2) {
		printf("Wrong offset : %d\n", offset);
		return -12;
	}

	while ( ! feof(f)  ) {
		fscanf(f, "%c", &val);
		if (val == '\n' || val == '\t') break;
		tape_set(machine->tape, offset++, val);
	}
	return 0;
}

int turing_read(TURING* machine, FILE* fprog, FILE* fstring) {
	if ( prog_read(machine, fprog)) {
		printf("Cannot read prog\n");
		return -1;
	}
	if ( tape_read(machine, fstring)) {
		printf("Cannot read tape\n");
		return -2;
	}
	return 0;
}


TURING* turing_create() {
	// allocating parent structure
	TURING* machine = malloc(sizeof(TURING));
	if ( ! machine ) {
		printf("Cannot allocate mem for machine\n");
		goto err_nomachine;
	}
	// allocating prog
	machine->prog = prog_alloc(STMTS_MAX);
	if ( !machine->prog ) {
		printf("Cannot alloc mem for prog\n");
		goto err_noprog;
	}
	// allocating tape
	machine->tape = malloc(TAPE_MAX);
	if ( ! machine->tape ) {
		printf("Cannot alloc tape \n");
		goto err_notape;
	}
	memset(machine->tape, ' ', TAPE_MAX);
	
	machine->q = 1;
	machine->pos = 0;
	machine->pos_min = 0;
	machine->pos_max = 0;
	return machine;

err_notape:
	prog_free(machine->prog);
err_noprog:
	free(machine);
err_nomachine:
	return NULL;	
}

int statement_find(TURING* machine, char r, STATEMENT** result) {
	int stmt;
	for (stmt = 0; stmt < machine->prog->num; stmt++ ) {
		STATEMENT *s = &machine->prog->statements[stmt];
		if (s->r == r && machine->q == s->q ) {
			*result = s;
			return 0;
		}
	}
	return -10;

}


void turing_print(TURING* machine) {
	int pos;
	printf("Tape range: %d:%d, state=%d\n", 
		machine->pos_min,
		machine->pos_max,
		machine->q);

	for ( pos = machine->pos_min; pos <= machine->pos_max; pos++ ) {
		char r;
		tape_get(machine->tape, pos, &r);
		putchar(r);
	}
	putchar('\n');
	for ( pos = machine->pos_min; pos <= machine->pos_max; pos++ ) {
		putchar( pos == machine->pos? '^' : ' ');
	}
	putchar('\n');

}

int turing_step(TURING* machine) {
	char r;
	int err = 0;
	STATEMENT *s;
	err = tape_get(machine->tape, machine->pos, &r);
	if ( err ) return err;
	
	err = statement_find(machine, r, &s);
	if ( err ) return err;
	
	err = tape_set(machine->tape, machine->pos, s->w);
	if ( err ) return err;
	
	machine->q = s->qn;

	machine->pos += s->dir < 0? -1 : 1;

	if ( machine->pos < machine->pos_min ) 
		machine->pos_min = machine->pos;
	if ( machine->pos > machine->pos_max ) 
		machine->pos_max = machine->pos;
	
	return err;	
}

int turing_interpret(TURING* machine) {
	int err = 0;
	int steps = 0;
	turing_print(machine);	
	while ( !(err = turing_step(machine))) {
 		printf("step %d\n", steps);
		turing_print(machine);
		steps++;
	}
	
	printf("Machine finished after %d steps with %d error\n", steps, err);
	return err;
}

void turing_free(TURING* machine) {
}


int turing(char* prog, char* string) {
	FILE* fprog;
	FILE* fstring;
	TURING* machine;
	int err;
	
	machine = turing_create();
	if (! machine) {
		printf("Error while creating machine\n");
		err = -1;
		goto err_nomachine;
	}

	fprog = fopen(prog, "r");
	if ( ! fprog ) {
		printf("Cannot open prog file %s!\n", prog);
		err = -2;
		goto err_nofprog;
	}

	fstring = fopen(string, "r");
	if ( ! fstring ) {
		printf("Cannot open string file %s!\n", string);
		err = -3;
		goto err_nofstring;
	}

	err = turing_read(machine, fprog, fstring);
	if (err) {
		printf("Error while read: %d\n", err);
		goto err_read;
	}
	err = turing_interpret(machine);

err_read:
	fclose(fstring);
err_nofstring:
	fclose(fprog);
err_nofprog:
	turing_free(machine);
err_nomachine:
	return err;
}

int main(int argc, char* argv[]) {
	if ( argc < 3 ) {
		printf("Wrong number of args\n");
		printf("Usage: %s <prog> <string>\n", argv[0]);
		return -1;
	}
	
	return turing(argv[1], argv[2]);
}
